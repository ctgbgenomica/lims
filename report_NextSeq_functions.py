from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.lib.units import cm, inch
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT, TA_JUSTIFY
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

from xml.etree import ElementTree
from datetime import date
from collections import OrderedDict

import os, itertools, pandas as pd

from interop import py_interop_run_metrics, py_interop_run, py_interop_summary

#~ Derived from interop tutorial for parse_illumina_binData: 
#~ Since the value may or may not define the mean, standard deviation, median statistics, we define a simple function to detect whether it does and then format it appropriately.
def format_value(val):
	if hasattr(val, 'mean'):
		return format(val.mean(),'.2f')
	else:
		return format(val,'.2f')

def getSequencingInfo(xmlFile):
	with open(xmlFile, 'rt') as f:
		tree = ElementTree.parse(f)
	for element in tree.iter("RunParameters"):
		experimentName = element.find('ExperimentName').text
		ncData = element.find('RunStartDate').text
	yy = ncData[:2]
	mm = ncData[2:4]
	dd = ncData[4:]
	experimentData = "%s/%s/%s" % (dd,mm,yy)
	return(experimentName, experimentData)

def parse_illumina_binData(run_folder,theoric_values):
	run_metrics = py_interop_run_metrics.run_metrics()
	valid_to_load = py_interop_run.uchar_vector(py_interop_run.MetricCount, 0)
	py_interop_run_metrics.list_summary_metrics_to_load(valid_to_load)
	print (py_interop_run_metrics.list_summary_metrics_to_load(valid_to_load))
	run_folder = run_metrics.read(run_folder, valid_to_load)
	summary = py_interop_summary.run_summary()
	py_interop_summary.summarize_run_metrics(run_metrics, summary)
	columns = [('Lane', 'lane'), ('% Q30', 'percent_gt_q30'), ('Glob. Density','density'),('Phasing','phasing'),('Prephasing','prephasing'),('Reads','reads'),('Reads P.F.','reads_pf'),('Density PF', 'density_pf')]
	
	d_R1 = []
	d_R2 = []
	
	rows_R1 = [summary.at(0).at(lane) for lane in range(summary.lane_count())]
	rows_R2 = [summary.at(3).at(lane) for lane in range(summary.lane_count())]

	for label, func in columns:
		d_R1.append((label, pd.Series([format_value(getattr(r, func)()) for r in rows_R1])))
		d_R2.append((label, pd.Series([format_value(getattr(s, func)()) for s in rows_R2])))
		
	df_R1 = pd.DataFrame.from_items(d_R1)
	df_R2 = pd.DataFrame.from_items(d_R2)
	
	list_values_R1 = [df_R1.columns[:,].values.astype(str).tolist()] + df_R1.values.tolist()
	list_values_R2 = [df_R2.columns[:,].values.astype(str).tolist()] + df_R2.values.tolist()
	
	list_values_R1.pop(0)
	list_values_R2.pop(0)
	
	for a in list_values_R1:
		a[0] = int(float(a[0]))
		a[6] = int(float(a[6]))
		a[5] = int(float(a[5]))
		pfpercent = str(format(((float(a[-1])/float(a[2])) * 100),'.2f'))
		phas_preph = "%s/%s" % (a[3],a[4])
		a[2] = str(round(float(a[2])/1000))
		a.insert(3,pfpercent)
		a[4] = phas_preph
		del a[5]
		del a[-1]
	
	for b in list_values_R2:
		b[0] = int(float(b[0]))
		b[6] = int(float(b[6]))
		b[5] = int(float(b[5]))
		pfpercent = str(format(((float(b[-1])/float(b[2])) * 100),'.2f'))
		phas_preph = "%s/%s" % (b[3],b[4])
		b[2] = str(round(float(b[2])/1000))
		b.insert(3,pfpercent)
		b[4] = phas_preph
		del b[5]
		del b[-1]
	
	header = ['Lane', '% Q30','Glob Density',"Clusters P.F",'Phas/Prephas','Reads','Reads P.F.']
	list_values_R1.append(theoric_values)
	list_values_R1.insert(0,header)
	
	list_values_R2.append(theoric_values)
	list_values_R2.insert(0,header)

	return list_values_R1, list_values_R2

def get_theorical_values(exp_name):
	theoric_dic = {
	"BRCA"	: 	["Ref","NA","865-965",">85%","0<x<0.4","NA","NA" ],
	"CARDIO" : 	["Ref",">85%","1000-1200",">75%","0<x<0.4","NA","NA"],
	"CANCER" : 	["Ref",">85%","1000-1200",">75%","0<x<0.4","NA","NA"],
	"CANCAR" : 	["Ref",">85%","1000-1200",">75%","0<x<0.4","NA","NA"],
	"TSONE"  : 	["Ref",">80%","170-220",">75%","0<x<0.4","NA","NA"],
	"Not Available" : ["Ref","NA","NA","NA","0<x<0.4","NA","NA"]
	}
	upper_name = exp_name.split("_")[0].upper()
	try:
		theoric_values = theoric_dic[upper_name]
	except KeyError:
		theoric_values = theoric_dic["Not Available"]
	return theoric_values

def read_coverage_file(metrics_file):
	mf_list = []
	metrics_dic = {}
	general_list = []
	coverage_list = []
	with open(metrics_file) as mf:
		for j in itertools.islice(mf, 6, 8):
			values = [j.replace("\n","") for j in j.split("\t")]
			mf_list.append(values)
	columns = mf_list[0]
	rows = mf_list[1]
	for element in range(0, len(columns)):
		metrics_dic[columns[element]] = rows[element]
		#~ print columns[element], rows[element]
	general_keys = {'TOTAL_READS':'Total sequenced reads',
					'PF_READS': 'Reads Passing Filters',
					'PCT_PF_UQ_READS_ALIGNED': '% Aligned Reads',
					'MEAN_TARGET_COVERAGE': 'Mean Coverage',
					'ZERO_CVG_TARGETS_PCT': '% 0 coverage bases'}
	coverage_keys = {'PCT_TARGET_BASES_20X': '% Coverage 20X',
					 'PCT_TARGET_BASES_30X': '% Coverage 30X',
					 'PCT_TARGET_BASES_40X': '% Coverage 40X',
					 'PCT_TARGET_BASES_50X': '% Coverage 50X',
					 'PCT_TARGET_BASES_100X': '% Coverage 100X'}
	for k in general_keys.keys():
		if k == 'PCT_PF_UQ_READS_ALIGNED' or k == 'ZERO_CVG_TARGETS_PCT':
			mv = float(metrics_dic[k]) * 100
			mv = format(mv, '.2f')
			list_to_append = [general_keys[k],mv]
		elif k == 'MEAN_TARGET_COVERAGE':
			mc = round(float(metrics_dic[k]))
			list_to_append = [general_keys[k],mc]
		else:
			list_to_append = [general_keys[k],metrics_dic[k]]
		general_list.append(list_to_append)
	for z in coverage_keys.keys():
		cv = float(metrics_dic[z]) * 100
		cv = format(cv, '.2f')
		list_to_append = [coverage_keys[z],str(cv)]
		coverage_list.append(list_to_append)
	return general_list, coverage_list

def all_sample_coverage(metrics_dir,hs_metrics_file_list):
	sample_coverage_list = []
	coverage_dic = {}
	#~ For reportlab I need a list of lists, not dictionaries
	coverage_list = []
	for sample_name in hs_metrics_file_list:
		hs_file = os.path.join(metrics_dir,"hs_metrics",sample_name)
		with open(hs_file) as mf:
			hs_metrics_dic = {}
			mf_list = []
			for j in itertools.islice(mf, 6, 8):
				values = [j.replace("\n","") for j in j.split("\t")]
				mf_list.append(values)
			#~ To obtain all data of hsMetrics file in a dictionary
			#~ for column in range(0, len(mf_list[0])):
				#~ print column, mf_list[1][column]
				#~ hs_metrics_dic[mf_list[0][column]] = mf_list[1][column]
			#~ coverage_dic[sample_name] = hs_metrics_dic
			#~ I need only the coverage value
			coverage_dic[sample_name.split("_")[0]] = mf_list[1][22]
	only_cov_list = []
	for S in coverage_dic.keys():
		element = [S,round(float(coverage_dic[S]))]
		coverage_list.append(element)
		only_cov_list.append(float(coverage_dic[S]))
	min_cov = round(min(only_cov_list))
	max_cov = round(max(only_cov_list))
	mean_cov = format(sum(only_cov_list)/len(only_cov_list), '.2f')
	global_value_to_append = [min_cov,max_cov,mean_cov]
	header_global_value = ['Min Coverage', 'Max Coverage', 'Mean Coverage']
	header_all_sample_coverage = ['Sample', 'Cov.']
	coverage_list.insert(0,header_all_sample_coverage)
	global_cov_value_list = []
	global_cov_value_list.append(header_global_value)
	global_cov_value_list.append(global_value_to_append)
	return (global_cov_value_list, coverage_list)

def create_sample_Document(logoImage,out_filename,patient,runData,expID,general_data, coverage_data,q_hist_plot):
	today = date.today()
	today = today.strftime("%d/%m/%y")
	#~ canvas object creation
	c = canvas.Canvas(out_filename)
	#~ print c.getAvailableFonts()
	#~ San Raffaele header
	c.drawImage(logoImage,230,750,width=3*cm,height=2.2*cm,mask=None)
	c.setFont('Courier-Bold', 18)
	c.drawString(100,722,"Laboratorio di Biologia Molecolare Clinica") ## In the middle of the page!
	c.setFont('Courier-Bold', 14)
	c.drawString(200,700,"Ospedale San Raffaele") ## In the middle of the page!
	c.setLineWidth(1)
	c.line(30,680,570,680)
	#~ General report information: patient, run name, report data generation 
	t = c.beginText()
	t.setTextOrigin(30,660)
	t.setFont('Courier',12)
	text_info = """
					Corsa: %s
	Il presente documento rappresenta un report di qualita' per l'esame genetico 
	avvenuto tramite tecniche di Next Generation Sequencing.
	Numero caso %s
	Data sequenziamento: %s
	Il documento e' stato generato in data: %s\n
	""" % (expID.upper(),patient,runData,today)
	t.setLeading(13)
	t.textLines(text_info)
	c.drawText(t)
	#~ Setting style for tables:
	table_style = [ ('LEADING',(0,0),(-1,-1),12),
					('FACE', (0,0), (-1,-1), 'Courier'),
					('SIZE',(0,0), (-1,-1),10),
					('GRID',(0,0),(-1,-1),1,colors.grey),
					('ALIGNMENT',(0,0),(-1,-1),'CENTRE'),
					('VALIGN',(0,-1),(-1,-1),'MIDDLE')]
	#~ Header for the first part
	header_table1 = c.beginText()
	header_table1.setTextOrigin(240,570)
	header_table1.setFont('Courier-Bold',12)
	header_table1.setFillColorRGB(0.132,0.54,0.132)
	header_table1_text = "Metriche Paziente"
	header_table1.textLines(header_table1_text)
	c.drawText(header_table1)
	#~ First table: general metrics	
	general_metrics_table = Table(general_data)
	general_metrics_table.setStyle(table_style)
	general_metrics_table.wrapOn(c,100,200)
	general_metrics_table.drawOn(c, 50, 470, cm)
	alert_table1 = c.beginText()
	alert_table1.setTextOrigin(70,461)
	alert_table1.setFont('Courier-Bold',7)
	alert_table1.setFillColorRGB(0,0,0)
	alert_table1_text = """* Per coverage si intende il valore di 
							        TARGET COVERAGE"""
	alert_table1.textLines(alert_table1_text)
	c.drawText(alert_table1)
	#~ Second table: coverage metrics
	coverage_metrics_table = Table(sorted(coverage_data))
	coverage_metrics_table.setStyle(table_style)
	coverage_metrics_table.wrapOn(c,100,200)
	coverage_metrics_table.drawOn(c, 350, 470, cm)
	#~ Quality plots
	c.showPage()
	c.save()

def create_run_Document(logoImage, out_filename,runData,expID,R1_data,R2_data,cluster_count_hist,q_hist_plot,global_cov_table, sample_coverage_table):
	today = date.today()
	today = today.strftime("%d/%m/%y")
	#~ canvas object creation
	c = canvas.Canvas(out_filename)
	#~ print c.getAvailableFonts()
	#~ San Raffaele header
	c.drawImage(logoImage,230,750,width=3*cm,height=2.2*cm,mask=None)
	c.setFont('Courier-Bold', 14)
	c.drawString(100,740,"Laboratorio di Biologia Molecolare Clinica") ## In the middle of the page!
	c.setFont('Courier-Bold', 13)
	c.drawString(200,725,"Ospedale San Raffaele") ## In the middle of the page!
	c.setLineWidth(1)
	c.line(30,720,570,720)
	#~ General report information: patient, run name, report data generation 
	t = c.beginText()
	t.setTextOrigin(30,700)
	t.setFont('Courier',12)
	text_info = """
				Nome corsa: %s
	Il presente documento rappresenta un report di qualita' per l'analisi 
	di sequenziamento NGS avvenuta in data %s.
	Il documento e' stato generato in data: %s\n
	""" % (expID.upper(),runData,today)
	t.setLeading(13)
	t.textLines(text_info)
	c.drawText(t)
	#~ Setting style for tables:
	table_style = [ ('LEADING',(0,0),(-1,-1),12),
					('FACE', (0,0), (-1,-1), 'Courier'),
					('SIZE',(0,0), (-1,-1),10),
					('GRID',(0,0),(-1,-1),1,colors.grey),
					('ALIGNMENT',(0,0),(-1,-1),'CENTRE'),
					('VALIGN',(0,-1),(-1,-1),'MIDDLE')]
	#~ Header for the second part
	header_table2 = c.beginText()
	header_table2.setTextOrigin(210,645)
	header_table2.setFont('Courier-Bold',12)
	header_table2.setFillColorRGB(0.132,0.54,0.132)
	header_table2_text = "Metriche di sequenziamento"
	header_table2.textLines(header_table2_text)
	c.drawText(header_table2)
	table_style_reads = [ ('LEADING',(0,0),(-1,-1),12),
					('FACE', (0,0), (-1,-1), 'Courier'),
					('SIZE',(0,0), (-1,-1),10),
					('GRID',(0,0),(-1,-1),1,colors.grey),
					('ALIGNMENT',(0,0),(-1,-1),'CENTRE'),
					('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
					('BACKGROUND',(0,-1),(-1,-1),colors.HexColor(0x8fbc8f)),
					('FACE', (0,-1), (-1,-1), 'Courier-Bold')]
	#~ First Table: R1 data
	R1_string = c.beginText()
	R1_string.setTextOrigin(20,570)
	R1_string.setFont('Courier-Bold',12)
	R1_string.setFillColorRGB(1,0,0)
	R1_string_text = "R1"
	R1_string.textLines(R1_string_text)
	c.drawText(R1_string)
	R1_table = Table(R1_data)
	R1_table.setStyle(table_style_reads)
	R1_table.wrapOn(c,100,200)
	R1_table.drawOn(c,50,530)
	#~ Second table: R2 data
	R2_string = c.beginText()
	R2_string.setTextOrigin(20,459)
	R2_string.setFont('Courier-Bold',12)
	R2_string.setFillColorRGB(1,0,0)
	R2_string_text = "R2"
	R2_string.textLines(R2_string_text)
	c.drawText(R2_string)
	R2_table = Table(R2_data)
	R2_table.setStyle(table_style_reads)
	R2_table.wrapOn(c,100,200)
	R2_table.drawOn(c,50,415)
	#~ Third table(s): coverage data for all samples 
	#~ when samples are more than 12 split in 2 tables
	coverage_all_samples_table_style = [ ('LEADING',(0,0),(-1,-1),12),
					('FACE', (0,0), (-1,-1), 'Courier'),
					('SIZE',(0,0), (-1,-1),10),
					('GRID',(0,0),(-1,-1),1,colors.grey),
					('ALIGNMENT',(0,0),(-1,-1),'CENTRE'),
					('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
					('BACKGROUND',(0,0),(-1,0),colors.HexColor(0x8fbc8f)),
					('FACE', (0,0), (-1,0), 'Courier-Bold')]
	global_metrics_table_style = [ ('LEADING',(0,0),(-1,-1),12),
					('FACE', (0,0), (-1,-1), 'Courier'),
					('SIZE',(0,0), (-1,-1),10),
					('GRID',(0,0),(-1,-1),1,colors.grey),
					('ALIGNMENT',(0,0),(-1,-1),'CENTRE'),
					('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
					('BACKGROUND',(0,0),(-1,0),colors.HexColor(0x8fbc8f)),
					('FACE', (0,0), (-1,0), 'Courier-Bold')]
	#~ On NextSeq we run max 24 sample per time (9 samples for exome sequencing)
	if len(sample_coverage_table) > 13:
		table_sample_1 = sample_coverage_table[:13]
		table_sample_2 = sample_coverage_table[13:]
		table_sample_2.insert(0,['Sample', 'Cov.'])
		st1 = Table(table_sample_1,colWidths=[60,60])
		st2 = Table(table_sample_2,colWidths=[60,60])
		st1.setStyle(coverage_all_samples_table_style)
		st2.setStyle(coverage_all_samples_table_style)
		st1.wrapOn(c,100,200)
		st2.wrapOn(c,100,200)
		st1.drawOn(c,50,155)
		st2.drawOn(c,180,155)
		#~ Quality plots
		c.drawImage(cluster_count_hist,350,210,width=7*cm,height=6.5*cm,mask=None)
		c.drawImage(q_hist_plot,350,22,width=7*cm,height=6.5*cm,mask=None)
		#~ Fourth table: global coverage data: max min and mean
		metrics_cov_table = Table(global_cov_table)
		metrics_cov_table.setStyle(global_metrics_table_style)
		metrics_cov_table.wrapOn(c,100,200)
		metrics_cov_table.drawOn(c,50,100)
		alert_tablecov = c.beginText()
		alert_tablecov.setTextOrigin(50,93)
		alert_tablecov.setFont('Courier-Bold',7)
		alert_tablecov.setFillColorRGB(0,0,0)
		alert_tablecov_text = """* Per coverage si intende il valore di TARGET COVERAGE"""
		alert_tablecov.textLines(alert_tablecov_text)
		c.drawText(alert_tablecov)
	else: 
		table_sample_1 = sample_coverage_table
		st1 = Table(table_sample_1,colWidths=[90,90])
		st1.setStyle(coverage_all_samples_table_style)
		st1.wrapOn(c,100,200)
		st1.drawOn(c,50,175)
		c.drawImage(cluster_count_hist,350,210,width=7*cm,height=6.5*cm,mask=None)
		c.drawImage(q_hist_plot,350,22,width=7*cm,height=6.5*cm,mask=None)
	#~ Fourth table: global coverage data: max min and mean
		metrics_cov_table = Table(global_cov_table)
		metrics_cov_table.setStyle(global_metrics_table_style)
		metrics_cov_table.wrapOn(c,100,200)
		metrics_cov_table.drawOn(c,50,100)
		alert_tablecov = c.beginText()
		alert_tablecov.setTextOrigin(50,93)
		alert_tablecov.setFont('Courier-Bold',7)
		alert_tablecov.setFillColorRGB(0,0,0)
		alert_tablecov_text = """* Per coverage si intende il valore di TARGET COVERAGE"""
		alert_tablecov.textLines(alert_tablecov_text)
		c.drawText(alert_tablecov)
	c.showPage()
	c.save()

def createDocument_HTML(logoImage, out_filename,patient,runData,expID):
	today = date.today()
	today = today.strftime("%d/%m/%y")
	doc = SimpleDocTemplate(out_filename)
	Story=[]
	logo = logoImage
	im = Image(logo,width=3*cm,height=2.2*cm)
	im.hAlign = 'CENTER'
	Story.append(im)
	styles=getSampleStyleSheet()
	styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
	ptext = '<font size=16>Laboratorio di Biologia Molecolare Clinica</font>'
	Story.append(Paragraph(ptext, styles["Center"]))
	Story.append(Spacer(1, 9))
	ptext = '<font size=14>Ospedale San Raffaele</font>'
	Story.append(Paragraph(ptext, styles["Center"]))
	Story.append(Spacer(1, 20))
	styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
	ptext = "<font size=12>Il presente documento rappresenta un report di qualita' per l'esame genetico avvenuto tramite tecniche di Next Generation Sequencing</font>"
	Story.append(Paragraph(ptext, styles["Justify"]))
	Story.append(Spacer(1, 6))
	ptext = "<font size=12>Numero caso %s</font>" % patient
	Story.append(Paragraph(ptext, styles["Justify"]))
	Story.append(Spacer(1, 6))
	ptext = "<font size=12>Data sequenziamento: %s</font>" % runData
	Story.append(Paragraph(ptext, styles["Justify"]))
	Story.append(Spacer(1, 6))
	ptext = "<font size=12>Nome corsa: %s</font>" % expID.upper()
	Story.append(Paragraph(ptext, styles["Justify"]))
	Story.append(Spacer(1, 6))
	ptext = "<font size=12>Il documento e' stato generato in data: %s</font>" % today
	Story.append(Paragraph(ptext, styles["Justify"]))
	Story.append(Spacer(1, 6))
	
	doc.build(Story)




