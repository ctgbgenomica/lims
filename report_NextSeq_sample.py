from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.lib.units import cm, inch
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT, TA_JUSTIFY
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

from xml.etree import ElementTree
from datetime import date
from collections import OrderedDict

import os, itertools,argparse, pandas as pd

from interop import py_interop_run_metrics, py_interop_run, py_interop_summary
from report_NextSeq_functions import *

def main(metrics_file,patient_full,seq_run,analysis_run):
	logo = "/storage/snakemake-pipelines/OSR_LOGO.png"
	RunParameters = os.path.join(seq_run, "RunParameters.xml")
	patient = patient_full.split("_")[0]
	filename = os.path.join(analysis_run, "Reports/%s.pdf" % (patient_full))
	#~ filename = "%s.pdf" % (patient)

	#~ metrics_file = os.path.join(analysis_run,"hs_metrics/",patient_full)
	expID,runData = getSequencingInfo(RunParameters)
	#~ print (expID,runData)
	theoric_values = get_theorical_values(expID)
	#~ print (theoric_values)
	general_data, coverage_data = read_coverage_file(metrics_file)
	#~ print (seq_run)
	#~ R1,R2 = parse_illumina_binData(seq_run,theoric_values)

	#~ cluster_count_plot_name = "%s%s" % (os.path.basename(seq_run),"_ClusterCount-by-lane.png")
	q_histogram_plot_name = "%s%s" % (os.path.basename(seq_run),"_q-histogram.png")
	#~ plot1 = os.path.join(analysis_run,cluster_count_plot_name)
	plot2 = os.path.join(analysis_run,q_histogram_plot_name)
	
	create_sample_Document(logo,filename,patient,runData,expID,general_data,coverage_data,plot2)
	#~ print (logo,filename,patient,runData,expID,general_data,coverage_data,R1,R2,plot1,plot2)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-pt", "--patient_file",action='store',required=True,help="Picard metrics file")
	parser.add_argument("-id", "--patient_id",action='store',required=True,help="Sample ID")
	parser.add_argument("-sr", "--sequencing-run",action='store',required=False,help="Absolute path of the run")
	parser.add_argument("-ar", "--analysis-run",action='store',required=False,help="Absolute path of the analysis")
	args = parser.parse_args()
	main(args.patient_file,args.patient_id,args.sequencing_run,args.analysis_run)




